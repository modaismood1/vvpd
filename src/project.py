import random
from random import randint


def start():
    print('Что вы хотите сделать?')
    while True:
        try:
            action = input('Введите 1, если хотите ввести значения с клавиатуры.' '\n'
                           'Введите 2, если хотите получить случайные значения.' '\n')
            action = int(action)
            if action == 1 or action == 2:
                break
            else:
                print('Введите 1 или 2.')
        except ValueError:
            print('Введедено недопустимое значение')

    return action


def value_keyboard(value):
    while True:
        try:
            val = list(input('Введите значения списка через пробел: ').split())
            for i in range(len(val)):
                g = int(val[i])
            break
        except ValueError:
            print('Введедены недопустимые значения, введите числа.')
    value = []

    for i in range(len(val)):
        v = int(val[i])
        value.append(v)

    return value


def values_random(value):
    while True:
        try:
            n = input('Введите натуральное число '
                      'равное количеству чисел в списке:')
            n = int(n)
            if n > 0:
                break
            else:
                print('Условие не выполнено: n < 0')
        except ValueError:
            print('Введедено недопустимое значение')

    print('Введите интервал целых чисел [a, b], '
          'которые будут входить в список:')
    while True:
        try:
            a = input('Число a = ')
            b = input('Число b = ')
            a = int(a)
            b = int(b)
            if a < b:
                break
            else:
                print('Условие не выполнено: a < b !')
        except ValueError:
            print('Введедено недопустимое значение')

    value = []

    for i in range(n):
        value.append(randint(a, b))

    return value


def function_selection(v):
    while True:
        try:
            print('Что вы хотите сделать?')
            v = input('Введите 1, если хотите найти индекс элемента.' '\n'
                      'Введите 2, если хотите найти минимальный элемент списка.' '\n'
                      'Введите 3, если хотите отсортировать список.'  '\n'
                      'Введите 4, если хотите найти самую длинную ' 
                      'последовательность одинаковых чисел.' '\n'
                      'Введите 5, если хотите найти 3 наибольших элемента.' '\n'
                      'Введите 6, если хотите сравнить 2 списка.' '\n'
                      'Введите 7, если хотите изменить список.' '\n')
            v = int(v)
            if 0 < v <= 7:
                break
            else:
                print('Введедено неверное значение')

        except ValueError:
            print('Введедено недопустимое значение')

    return v


def first_function(value):
    while True:
        try:
            desired_value = input('Введите число: ')
            desired_value = int(desired_value)
            break
        except ValueError:
            print('Введедено недопустимое значение')
    for i in range(len(value)):
        if desired_value == value[i]:
            print('Индексы числа', desired_value, ': ', i)

    if desired_value not in value:
        print('Такого значения нет в списке.')


def second_function(value):
    min_value = min(value)
    print('Минимальное число:', min_value)
    for g in range(len(value)):
        if min_value == value[g]:
            print('Индексы минимального числа в списке: ', g)
            return g


def third_function(value):
    while True:
        try:
            v = input('Отсортировать в порядке возрастания - 1 '
                      'или в порядке убывания - 2: ')
            v = int(v)
            if v == 1 or v == 2:
                break
            else:
                print('неверное значение.')
        except ValueError:
            print('Введедено недопустимое значение')
    if v == 1:
        value = sorted(value)
    else:
        value = sorted(value, reverse=True)
    print(value)

    return value


def fourth_function(value):
    result = 1
    max_result = 0
    last_seen = value[0]

    for i in value[1:]:
        if i == last_seen:
            result += 1
        else:
            if result > max_result:
                max_result = result
            last_seen = i
            result = 1

    if result > max_result:
        max_result = result

    print('Максимальная длина одинаковых чисел: ', max_result)
    return max_result


def fifth_function(value):
    value = sorted(value, reverse=True)
    val = []
    for i in range(len(value)-1):
        index = i + 1
        if value[i] != value[index]:
            val.append(value[i])
    val.append(value[-1])

    value = []

    if len(val) >= 3:
        value.append(val[0])
        value.append(val[1])
        value.append(val[2])
        print(value)
    else:
        print(val)
    return value


def sixth_function(value):
    while True:
        try:
            v = list(input('Введите числа через пробел, которые'
                           ' хотите сравнить с первым списком:').split())
            for i in range(len(v)):
                v[i] = int(v[i])
            break
        except ValueError:
            print('Введены недопустимые значения')

    if value == v:
        print('Списки одинаковые.')
    else:
        if len(value) > len(v):
            print('Первый список больше второго списка на: ', len(value) - len(v))
        elif len(value) < len(v):
            print('Второй список больше первого списка на: ', len(v) - len(value))
        else:
            print('Списки одинаковой длины')

        result1 = list(set(value) & set(v))
        if not result1:
            print('Одинаковых элементов нет.')
        else:
            print('Одинаковые элементы списка: ', result1)

        result2 = list(set(value) - set(v))
        if not result2:
            print('Элементов первого списка не входящие во второй список нет.')

        else:
            print('Элементы первого списка не входящие во второй: ', result2)

        result3 = list(set(v) - set(value))
        if not result3:
            print('Элементов второго списка не входящие в первый список нет.')
        else:
            print('Элементы второго списка не входящие в первый: ', result3)


def main():
    value = []
    action = start()

    if action == 1:
        value = value_keyboard(value)
    else:
        value = values_random(value)
        print('Список случайных чисел:')
        print(value)

    while True:
        v = []
        v = function_selection(v)
        if v == 1:
            first_function(value)
        elif v == 2:
            second_function(value)
        elif v == 3:
            third_function(value)
        elif v == 4:
            fourth_function(value)
        elif v == 5:
            fifth_function(value)
        elif v == 6:
            sixth_function(value)
        else:
            main()


if __name__ == '__main__':
    main()
